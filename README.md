PHP7-FPM & Nginx - Docker SDK
-------------------------------------

l'installazione dell'ambiente di sviluppo segue i seguenti passi:

1. clonare i sorgenti del progetto nella cartella ```../turisanda``` rispetto a questo file

2. configurare le copie locali di ```docker.env.yml```
```bash
$ cp ./docker/php-fpm7/docker.env.yml.dist ./docker/php-fpm7/docker.env.yml
```
2.1 sostituire ```WWW_USER_ID=10239 WWW_GROUP_ID=5513``` con i valori generati
da:
```bash
id
```
2.2 sostituire il valore ```XDEBUG_HOST_MACHINE_IP``` con l'ip della propria
macchina:
```bash
# docker0 inet addr
$ ifconfig
```
3. lanciare docker-compose per costruire le macchine
```bash
$ docker-compose up -d
```
4. controllare che l'installazione sia andata a buon fine
```bash
# p-fpm e p-nginx devono essere up e configurati sulle porte corrette
$ docker-compose ps
```
5. installare i vendor e il materiale necessario per il backend
```bash
$ docker exec -it t-fpm bash -l
# all'interno del container eseguire
$ composer install
```
6. aggiungi agli host i virtaulhost del progetto
```bash
# appendi al file /etc/hosts
127.0.0.1       turisanda.local
```
7. Test
```bash
$ open http://turisanda.local:9090/app_dev.php
$ http http://turisanda.local:9090/app_dev.php
```
