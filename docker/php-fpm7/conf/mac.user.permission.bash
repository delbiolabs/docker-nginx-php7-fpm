#!/usr/bin/env bash

# Tweaks to give Apache/PHP write permissions to the app

usermod -u ${BOOT2DOCKER_ID} www-data && \
usermod -G www-data www-data

groupmod -g $(($BOOT2DOCKER_GID + 10000)) $(getent group $BOOT2DOCKER_GID | cut -d: -f1)
groupmod -g ${BOOT2DOCKER_GID} www-data
