#!/usr/bin/env bash

php_fpm_ini_file=/etc/php/7.0/fpm/php.ini
php_cli_ini_file=/etc/php/7.0/cli/php.ini
php_cli_debug_launcher_file=/usr/local/bin/php-cli-debug

if [ ! -z "$PHP_INI_POST_MAX_SIZE" ]; then
    sed -i "s/^\(post_max_size\ =\).*/\1 $PHP_INI_POST_MAX_SIZE/" ${php_fpm_ini_file}
fi
if [ ! -z "$PHP_INI_UPLOAD_MAX_FILESIZE" ]; then
    sed -i "s/^\(upload_max_filesize\ =\).*/\1 $PHP_INI_UPLOAD_MAX_FILESIZE/" ${php_fpm_ini_file}
fi
