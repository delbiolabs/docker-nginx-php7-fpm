#!/usr/bin/env bash

if [ ! -z "$WWW_USER_ID" ]; then
    usermod -u $WWW_USER_ID www-data
fi

if [ ! -z "$WWW_GROUP_ID" ]; then
    groupmod -g $WWW_GROUP_ID www-data
fi
