#!/usr/bin/env bash

case "${OS}" in
"mac")
  bash /usr/local/src/mac.user.permission.bash
	;;
*)
  bash /usr/local/src/linux.user.permission.bash
  ;;
esac
