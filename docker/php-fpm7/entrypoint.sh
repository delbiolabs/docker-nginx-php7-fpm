#!/usr/bin/env bash

bash /usr/local/src/set-permission.bash
bash /usr/local/src/set-xdebug.bash
bash /usr/local/src/set-php.bash

# Start PHP7-FPM
/usr/sbin/php-fpm7.0 --nodaemonize
